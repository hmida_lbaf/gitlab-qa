image: ruby:2.6

services:
 - docker:19.03.0-dind

stages:
  - check
  - release
  - test
  - notify

variables:
  TEST_IMAGE: registry.gitlab.com/gitlab-org/gitlab-build-images:gitlab-qa
  DOCKER_DRIVER: overlay
  DOCKER_HOST: tcp://docker:2375
  QA_ARTIFACTS_DIR: $CI_PROJECT_DIR
  QA_CAN_TEST_GIT_PROTOCOL_V2: 'false'

cache:
  key: "ruby:2.6"
  paths:
    - vendor/ruby

before_script:
  - bundle version
  - bundle install --clean --jobs=$(nproc) --path=vendor --retry=3 --quiet && bundle check
  - if [ -n "$TRIGGERED_USER" ] && [ -n "$TRIGGER_SOURCE" ]; then
      echo "Pipeline triggered by $TRIGGERED_USER at $TRIGGER_SOURCE";
    fi

check:rubocop:
  stage: check
  image: $TEST_IMAGE
  script: bundle exec rubocop
  except:
    - triggers
  tags:
    - docker

check:rspec:
  stage: check
  image: $TEST_IMAGE
  script: bundle exec rspec
  except:
    - triggers
  tags:
    - docker

.test:
  stage: test
  image: $TEST_IMAGE
  tags:
    - docker
  artifacts:
    when: always
    expire_in: 10d
    paths:
      - ./gitlab-qa-run-*

.ce-qa:
  only:
    variables:
      - $RELEASE == null
      - $RELEASE =~ /gitlab-ce/

.ee-qa:
  only:
    variables:
      - $RELEASE == null
      - $RELEASE =~ /gitlab-ee/

.only-qa:
  except:
    variables:
      - $RELEASE

.high-capacity:
  tags:
    - docker
    - 7gb
    - triggered-packages

.knapsack-variables:
  variables:
    KNAPSACK_REPORT_PATH: "knapsack/master_report.json"
    KNAPSACK_TEST_FILE_PATTERN: "qa/specs/features/**/*_spec.rb"
    KNAPSACK_GENERATE_REPORT: "true"

.rspec-report-opts:
  variables:
    FILE_SAFE_JOB_NAME: $(echo $CI_JOB_NAME | sed 's/[ /]/_/g')
    RSPEC_REPORT_OPTS: "--format RspecJunitFormatter --out \"tmp/rspec-${CI_JOB_ID}.xml\" --format html --out \"tmp/rspec-${FILE_SAFE_JOB_NAME}.htm\" --color --format documentation"

.quarantine:
  allow_failure: true

ce:sanity-framework:
  script:
    - ./bin/expect_exit_code_and_text "exe/gitlab-qa Test::Instance::Image ${RELEASE:=CE} -- --tag framework" 1 "2 examples, 1 failure"
  extends:
    - .test
    - .high-capacity
    - .ce-qa

ee:sanity-framework:
  script:
    - ./bin/expect_exit_code_and_text "exe/gitlab-qa Test::Instance::Image ${RELEASE:=EE} -- --tag framework" 1 "2 examples, 1 failure"
  extends:
    - .test
    - .high-capacity
    - .ee-qa

ce:instance:
  script:
    - exe/gitlab-qa Test::Instance::Image ${RELEASE:=CE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .knapsack-variables
    - .rspec-report-opts
  parallel: 5

ce:instance-quarantine:
  script:
    - exe/gitlab-qa Test::Instance::Image ${RELEASE:=CE} -- --tag quarantine --tag ~orchestrated $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .quarantine
    - .rspec-report-opts

ee:instance:
  script:
    - exe/gitlab-qa Test::Instance::Image ${RELEASE:=EE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .knapsack-variables
    - .rspec-report-opts
  parallel: 5

ee:instance-quarantine:
  script:
    - exe/gitlab-qa Test::Instance::Image ${RELEASE:=EE} -- --tag quarantine --tag ~orchestrated $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .quarantine
    - .rspec-report-opts

ce:docker:
  script:
    - exe/gitlab-qa Test::Instance::Image ${RELEASE:=CE} -- --tag docker $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .rspec-report-opts

ee:docker:
  script:
    - exe/gitlab-qa Test::Instance::Image ${RELEASE:=EE} -- --tag docker $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .rspec-report-opts

ce:relative_url:
  script:
    - exe/gitlab-qa Test::Instance::RelativeUrl ${RELEASE:=CE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .knapsack-variables
    - .rspec-report-opts
  parallel: 5

ce:relative_url-quarantine:
  script:
    - exe/gitlab-qa Test::Instance::RelativeUrl ${RELEASE:=CE} -- --tag quarantine --tag ~orchestrated $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .quarantine
    - .rspec-report-opts

ee:relative_url:
  script:
    - exe/gitlab-qa Test::Instance::RelativeUrl ${RELEASE:=EE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .knapsack-variables
    - .rspec-report-opts
  parallel: 5

ee:relative_url-quarantine:
  script:
    - exe/gitlab-qa Test::Instance::RelativeUrl ${RELEASE:=EE} -- --tag quarantine --tag ~orchestrated $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .quarantine
    - .rspec-report-opts

ce:image:
  script:
    - exe/gitlab-qa Test::Omnibus::Image ${RELEASE:=CE}
  extends:
    - .test
    - .ce-qa

ee:image:
  script:
    - exe/gitlab-qa Test::Omnibus::Image ${RELEASE:=EE}
  extends:
    - .test
    - .ee-qa

ce:update:
  script:
    - exe/gitlab-qa Test::Omnibus::Update ${RELEASE:=CE} ${RELEASE:=CE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .rspec-report-opts
    - .knapsack-variables
  parallel: 5

ce:update-quarantine:
  script:
    - exe/gitlab-qa Test::Omnibus::Update ${RELEASE:=CE} ${RELEASE:=CE} -- --tag quarantine --tag ~orchestrated $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .quarantine
    - .rspec-report-opts

ee:update:
  script:
    - exe/gitlab-qa Test::Omnibus::Update ${RELEASE:=EE} ${RELEASE:=EE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .rspec-report-opts
    - .knapsack-variables
  parallel: 5

ee:update-quarantine:
  script:
    - exe/gitlab-qa Test::Omnibus::Update ${RELEASE:=EE} ${RELEASE:=EE} -- --tag quarantine --tag ~orchestrated $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .quarantine
    - .rspec-report-opts

ce:upgrade:
  script:
    - exe/gitlab-qa Test::Omnibus::Upgrade CE -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .only-qa
    - .rspec-report-opts
    - .knapsack-variables
  parallel: 5

ce:upgrade-quarantine:
  script:
    - exe/gitlab-qa Test::Omnibus::Upgrade CE -- --tag quarantine --tag ~orchestrated $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .only-qa
    - .quarantine
    - .rspec-report-opts

ee-previous-to-ce:update:
  script:
    - exe/gitlab-qa Test::Omnibus::Update EE CE -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .only-qa
    - .rspec-report-opts

ee-previous-to-ce:update-quarantine:
  script:
    - exe/gitlab-qa Test::Omnibus::Update EE CE -- --tag quarantine --tag ~orchestrated $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .only-qa
    - .quarantine
    - .rspec-report-opts

ce:mattermost:
  script:
    - exe/gitlab-qa Test::Integration::Mattermost ${RELEASE:=CE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .rspec-report-opts

ce:mattermost-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::Mattermost ${RELEASE:=CE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .quarantine
    - .rspec-report-opts

ee:mattermost:
  script:
    - exe/gitlab-qa Test::Integration::Mattermost ${RELEASE:=EE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .rspec-report-opts

ee:mattermost-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::Mattermost ${RELEASE:=EE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .quarantine
    - .rspec-report-opts

ee:geo:
  script:
    - exe/gitlab-qa Test::Integration::Geo ${RELEASE:=EE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .rspec-report-opts

ee:geo-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::Geo ${RELEASE:=EE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .quarantine
    - .rspec-report-opts

ce:ldap_no_tls:
  script:
    - exe/gitlab-qa Test::Integration::LDAPNoTLS ${RELEASE:=CE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .rspec-report-opts

ce:ldap_no_tls-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::LDAPNoTLS ${RELEASE:=CE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .quarantine
    - .rspec-report-opts

ee:ldap_no_tls:
  script:
    - exe/gitlab-qa Test::Integration::LDAPNoTLS ${RELEASE:=EE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .rspec-report-opts

ee:ldap_no_tls-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::LDAPNoTLS ${RELEASE:=EE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .quarantine
    - .rspec-report-opts

ce:ldap_tls:
  script:
    - exe/gitlab-qa Test::Integration::LDAPTLS ${RELEASE:=CE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .rspec-report-opts

ce:ldap_tls-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::LDAPTLS ${RELEASE:=CE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .quarantine
    - .rspec-report-opts

ee:ldap_tls:
  script:
    - exe/gitlab-qa Test::Integration::LDAPTLS ${RELEASE:=EE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .rspec-report-opts

ee:ldap_tls-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::LDAPTLS ${RELEASE:=EE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .quarantine
    - .rspec-report-opts

ee:ldap_no_server:
  script:
    - exe/gitlab-qa Test::Integration::LDAPNoServer ${RELEASE:=EE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .rspec-report-opts

ee:ldap_no_server-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::LDAPNoServer ${RELEASE:=EE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .quarantine
    - .rspec-report-opts

ce:instance_saml:
  script:
    - exe/gitlab-qa Test::Integration::InstanceSAML ${RELEASE:=CE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .rspec-report-opts

ce:instance_saml-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::InstanceSAML ${RELEASE:=CE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .quarantine
    - .rspec-report-opts

ee:instance_saml:
  script:
    - exe/gitlab-qa Test::Integration::InstanceSAML ${RELEASE:=EE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .rspec-report-opts

ee:instance_saml-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::InstanceSAML ${RELEASE:=EE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .quarantine
    - .rspec-report-opts

ee:group_saml:
  script:
    - exe/gitlab-qa Test::Integration::GroupSAML ${RELEASE:=EE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .rspec-report-opts

ee:group_saml-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::GroupSAML ${RELEASE:=EE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .quarantine
    - .rspec-report-opts

ce:kubernetes:
  script:
    - exe/gitlab-qa Test::Integration::Kubernetes ${RELEASE:=CE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .rspec-report-opts

ce:kubernetes-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::Kubernetes ${RELEASE:=CE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .quarantine
    - .rspec-report-opts

ee:kubernetes:
  script:
    - exe/gitlab-qa Test::Integration::Kubernetes ${RELEASE:=EE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .rspec-report-opts

ee:kubernetes-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::Kubernetes ${RELEASE:=EE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .quarantine
    - .rspec-report-opts

ce:object_storage:
  script:
    - exe/gitlab-qa Test::Integration::ObjectStorage ${RELEASE:=CE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .rspec-report-opts

ce:object_storage-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::ObjectStorage ${RELEASE:=CE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .quarantine
    - .rspec-report-opts

ee:object_storage:
  script:
    - exe/gitlab-qa Test::Integration::ObjectStorage ${RELEASE:=EE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .rspec-report-opts

ee:object_storage-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::ObjectStorage ${RELEASE:=EE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .quarantine
    - .rspec-report-opts

ce:oauth:
  script:
    - exe/gitlab-qa Test::Integration::OAuth ${RELEASE:=CE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .rspec-report-opts

ce:oauth-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::OAuth ${RELEASE:=CE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ce-qa
    - .quarantine
    - .rspec-report-opts

ee:oauth:
  script:
    - exe/gitlab-qa Test::Integration::OAuth ${RELEASE:=EE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .rspec-report-opts

ee:oauth-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::OAuth ${RELEASE:=EE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .quarantine
    - .rspec-report-opts

ee:packages:
  script:
    - exe/gitlab-qa Test::Integration::Packages ${RELEASE:=EE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .rspec-report-opts

ee:packages-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::Packages ${RELEASE:=EE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .quarantine
    - .rspec-report-opts

ee:elasticsearch:
  script:
    - exe/gitlab-qa Test::Integration::Elasticsearch ${RELEASE:=EE} -- $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .rspec-report-opts

ee:elasticsearch-quarantine:
  script:
    - exe/gitlab-qa Test::Integration::Elasticsearch ${RELEASE:=EE} -- --tag quarantine $RSPEC_REPORT_OPTS
  extends:
    - .test
    - .high-capacity
    - .ee-qa
    - .quarantine
    - .rspec-report-opts

.report-base:
  image: ruby:2.6-alpine
  variables:
    BASE_ARTIFACT_URL: "${CI_PROJECT_URL}/-/jobs/${CI_JOB_ID}/artifacts/file/"
    COMBINED_REPORT: combined-report.html
  artifacts:
    when: always
    paths:
      - gitlab-qa-run-*
      - combined-report.html
    reports:
      junit: gitlab-qa-run-*/**/rspec-*.xml
  allow_failure: true
  when: always
  before_script:
    - apk add --update build-base libxml2-dev libxslt-dev git && rm -rf /var/cache/apk/*
    - gem install nokogiri gitlab --no-document
    - cd gitlab-qa-run-*/gitlab-*
    - ARTIFACT_DIRS=$(pwd |rev| awk -F / '{print $1,$2}' | rev | sed s_\ _/_)
    - cd -
    - '[[ -f $COMBINED_REPORT ]] || echo "{}" > ${COMBINED_REPORT}'
    - bin/merge_html_reports ${COMBINED_REPORT} ${BASE_ARTIFACT_URL}${ARTIFACT_DIRS} gitlab-qa-run-*/**/rspec-*.htm

.notify_upstream_commit:
  stage: notify
  except:
    variables:
      - $TOP_UPSTREAM_SOURCE_PROJECT == null
      - $TOP_UPSTREAM_SOURCE_SHA == null

notify_upstream_commit:success:
  extends:
    - .report-base
    - .notify_upstream_commit
  script:
    - bin/notify_upstream_commit success ${COMBINED_REPORT}
  when: on_success

notify_upstream_commit:failure:
  extends:
    - .report-base
    - .notify_upstream_commit
  script:
    - bin/notify_upstream_commit failure ${COMBINED_REPORT}
  when: on_failure

# This job requires the `GITLAB_QA_ACCESS_TOKEN` and `GITLAB_QA_DEV_ACCESS_TOKEN`
# variable to be passed when triggered.
staging:
  script:
    - unset EE_LICENSE
    - exe/gitlab-qa Test::Instance::Staging
  extends:
    - .test
    - .high-capacity
    - .only-qa
  when: manual

release:
  stage: release
  rules:
    - if: '$CI_COMMIT_TAG'
      when: on_success
  script:
    - gem update --system
    - ruby --version
    - gem env version
    - gem build gitlab-qa.gemspec
    - gem push gitlab-qa*.gem
  artifacts:
    paths:
      - gitlab-qa*.gem
    expire_in: 30 days

.notify_slack:
  image: alpine
  stage: notify
  dependencies: []
  cache: {}
  before_script:
    - apk update && apk add git curl bash
  only:
    variables:
      - $TOP_UPSTREAM_SOURCE_JOB && $NOTIFY_CHANNEL

notify_slack:
  extends:
    - .notify_slack
  script:
    - bin/slack $NOTIFY_CHANNEL "☠️ Pipeline against $RELEASE failed! ☠️ See $CI_PIPELINE_URL (triggered from $TOP_UPSTREAM_SOURCE_JOB)" ci_failing
  when: on_failure
